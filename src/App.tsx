import {useEffect, useMemo, useState} from "react";
import axios from 'axios';

import Sidebar from './components/Sidebar';
import Cards from './components/Cards';

import classes from './App.module.scss';

export interface IItem {
    id: string;
    name: string;
    image: string;
    bgColor: string;
    tags: string[]
}


function App() {
    const [data, setData] = useState<IItem[]>([]);

    useEffect(() => {
        axios.get('https://logiclike.com/docs/courses.json').then(res => setData(res.data))
    }, []);


    const menus = useMemo(() => {
        const tags = (data || []).reduce((prev, item) => [...prev, ...item.tags], [] as string[]) || [];

        return [...new Set(tags)];
    }, [data])


    return (
        <div className={classes.wrapper}>
            hello muther father
            <Sidebar menus={['Все темы', ...menus]}/>
            <Cards items={data || []}/>
        </div>
    )
}

export default App