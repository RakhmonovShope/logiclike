import React from 'react';
import classes from './Button.module.scss';
import cx from 'clsx';

interface IProps {
    onClick: () => void;
    title: string;
    isActive: boolean;
}


const Button: React.FC<IProps> = ({onClick, isActive, title}) => {
    return (
        <div {...{onClick}} className={cx(classes.wrapper, isActive && classes.wrapperActive)}>
            {title}
        </div>
    );
};

export default Button;