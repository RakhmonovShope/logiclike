import React from 'react';
import classes from './Card.module.scss';

interface IProps {
    image: string;
    name: string;
    bgColor: string;
    onClick?: () => void;
}

const Card: React.FC<IProps> = ({image, name, bgColor, onClick}) => {
    return (
        <div className={classes.wrapper} onClick={() => onClick && onClick()} style={{
            '--bg-color': bgColor
        } as React.CSSProperties}>
            <div className={classes.image}>
                <div className={classes.imageInner}>
                    <img src={image} alt={name}/>
                </div>
            </div>
            <div className={classes.title}>
                {name}
            </div>
        </div>
    );
};

export default Card;