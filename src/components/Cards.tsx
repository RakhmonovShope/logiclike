import React from 'react';
import {IItem as IMenuItem} from '../App.tsx';


import {useQueryParams} from "../common/hooks";
import Card from './Card';

import classes from '../App.module.scss';

interface IProps {
    items: IMenuItem[]
}

const Cards: React.FC<IProps> = ({items}) => {
    const [query] = useQueryParams()

    return (
        <div className={classes.cards}>
            {items.map((item) => {
                if (item.tags.includes(query.activeMenu)) return <Card key={item.id} {...item}/>

                if (!query.activeMenu) return <Card key={item.id} {...item}/>
                return null;
            })}
        </div>
    );
};

export default Cards;