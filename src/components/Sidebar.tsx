import React from 'react';
import classes from '../App.module.scss';
import Button from './Button';

import {useQueryParams} from "../common/hooks";

interface IProps {
    menus: string[];
}

const Sidebar: React.FC<IProps> = ({menus}) => {
    const [query, {pushQuery}] = useQueryParams()

    return (
        <div className={classes.sidebar}>
            {menus.map((menu) => (
                <Button
                    key={menu}
                    title={menu}
                    isActive={query.activeMenu ? query.activeMenu === menu : menu === 'Все темы'}
                    onClick={() => {
                        if (menu === 'Все темы') {
                            pushQuery({activeMenu: ''});
                            return;
                        }

                        pushQuery({activeMenu: menu});
                    }}/>
            ))}
        </div>
    );
};

export default Sidebar;