import {createBrowserRouter} from 'react-router-dom';
import queryString from 'query-string';
import {QueryParamProvider} from 'use-query-params';
import {ReactRouter6Adapter} from 'use-query-params/adapters/react-router-6';

import DashboardPage from './App.tsx';

const router = createBrowserRouter([
    {
        path: "/",
        element: (
            <QueryParamProvider
                adapter={ReactRouter6Adapter}
                options={{
                    searchStringToObject: queryString.parse,
                    objectToSearchString: queryString.stringify
                }}
            >
                <DashboardPage/>
            </QueryParamProvider>
        )
    },
], {
    basename: '/logiclike/'
});

export default router;
